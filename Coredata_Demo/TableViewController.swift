//
//  TableViewController.swift
//  Coredata_Demo
//
//  Created by NguyenHoangSon on 11/1/18.
//  Copyright © 2018 NguyenHoangSon. All rights reserved.
//

import UIKit
import CoreData
class TableViewController: UITableViewController {
    var arrNumber : [Entity] = []
    override func viewDidLoad() {
        super.viewDidLoad()
       fetch()
        
    }
    
    func fetch() {
        arrNumber = try! AppDelegate.context.fetch(Entity.fetchRequest()) as [Entity] 
           
      
    }
    
    // MARK: - Table view data source

    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrNumber.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = arrNumber[indexPath.row].number

        return cell

    }
    
    
    @IBAction func addNewNubmer(sender: Any) {
        let entity = Entity(context: AppDelegate.context)
        entity.number = "3"
        AppDelegate.saveContext()
       fetch()

    }
    
}
